﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinControls
{
    [ToolboxBitmap("PictureLabel.bmp")]
    [Description("Компонент, представляющий Изображение с подписью")]
    [DefaultProperty("Style"), DefaultEvent("Paint")]
    public partial class PictureLabel : UserControl
    {
        public PictureLabel()
        {
            InitializeComponent();
            Height = 100;

            Picture = null;
            LabelText = "text";
        }

        /// <summary>
        /// Изображение, отображающееся в компоненте
        /// </summary>
        [Category("Компоненты"), Description("Изображение, отображающееся в компоненте")]
        
        public Image Picture
        {
            get { return PictureBox.Image; }
            set
            {
                PictureBox.Image = value;
            }
        }

        /// <summary>
        /// Текст метки, входящей в состав компонента
        /// </summary>
        [Category("Компоненты"), Description("Текст метки, входящей в состав компонента")]
        [DefaultValue(typeof(string), "text")]
        public string LabelText
        {
            get { return Label.Text; }
            set
            {
                Label.Text = value;
            }
        }

        bool selected = false;
        /// <summary>
        /// Указывает, выбран ли элемент управления
        /// </summary>
        [Category("Поведение"), Description("Указывает, выбран ли элемент управления")]
        [DefaultValue(false)]
        public bool Selected
        {
            get
            {
                return selected;
            }

            set
            {
                if (selected == value) return;

                selected = value;

                if (selected)
                {
                    BackColor = SystemColors.ActiveCaption;
                    Label.ForeColor = Color.White;
                }
                else
                {
                    BackColor = Parent.BackColor;
                    Label.ForeColor = Color.Black;
                }

                //Invalidate();
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            int dx = Label.Left - PictureBox.Right;

            PictureBox.Height = PictureBox.Width = Height - 20;

            int labelX = PictureBox.Right + dx;
            int labelY = (Height - Label.Height) / 2;
            Label.Location = new Point(labelX, labelY);
        }

        private void ChildElem_MouseClick(object sender, MouseEventArgs e)
        {
            OnMouseClick(e);
        }
    }
}
