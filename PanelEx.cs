﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using ImgModLib.Graphic;

namespace WinControls
{
    public partial class PanelEx : Panel
    {
        public Image Image { get; set; }

        public PanelEx()
        {
            InitializeComponent();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (Image == null) base.OnPaintBackground(e);
            //else OnPaint(e);

        }
    }
}
